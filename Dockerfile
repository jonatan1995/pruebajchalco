FROM openjdk:17-jdk-slim-buster
WORKDIR /app
COPY pruebanttdata-0.0.1-SNAPSHOT.jar build/
WORKDIR /app/build
ENTRYPOINT java -jar pruebanttdata-0.0.1-SNAPSHOT.jar