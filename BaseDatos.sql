--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

-- Started on 2023-04-30 13:00:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3344 (class 1262 OID 16398)
-- Name: dbtest; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE dbtest WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'Spanish_Ecuador.1252';


ALTER DATABASE dbtest OWNER TO postgres;

\connect dbtest

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 16400)
-- Name: tcliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcliente (
    cliente_id integer NOT NULL,
    direccion character varying(50),
    edad integer NOT NULL,
    genero character(1),
    identification character varying(16),
    nombre character varying(100),
    telefono character varying(10),
    contrasena character varying(50) NOT NULL,
    estado boolean NOT NULL
);


ALTER TABLE public.tcliente OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 16399)
-- Name: tcliente_cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tcliente_cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tcliente_cliente_id_seq OWNER TO postgres;

--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 214
-- Name: tcliente_cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tcliente_cliente_id_seq OWNED BY public.tcliente.cliente_id;


--
-- TOC entry 216 (class 1259 OID 16406)
-- Name: tcuenta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tcuenta (
    ccuenta character varying(50) NOT NULL,
    estado boolean NOT NULL,
    saldo_inicial double precision NOT NULL,
    tipo_cuenta character varying(20) NOT NULL,
    cliente_id integer NOT NULL
);


ALTER TABLE public.tcuenta OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16412)
-- Name: tmovimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tmovimiento (
    movimiento_id integer NOT NULL,
    estado boolean NOT NULL,
    fecha timestamp(6) without time zone NOT NULL,
    saldo double precision NOT NULL,
    tipo_movimiento character(1) NOT NULL,
    valor double precision NOT NULL,
    ccuenta character varying(50)
);


ALTER TABLE public.tmovimiento OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16411)
-- Name: tmovimiento_movimiento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tmovimiento_movimiento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tmovimiento_movimiento_id_seq OWNER TO postgres;

--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 217
-- Name: tmovimiento_movimiento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tmovimiento_movimiento_id_seq OWNED BY public.tmovimiento.movimiento_id;


--
-- TOC entry 3182 (class 2604 OID 16403)
-- Name: tcliente cliente_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcliente ALTER COLUMN cliente_id SET DEFAULT nextval('public.tcliente_cliente_id_seq'::regclass);


--
-- TOC entry 3183 (class 2604 OID 16415)
-- Name: tmovimiento movimiento_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tmovimiento ALTER COLUMN movimiento_id SET DEFAULT nextval('public.tmovimiento_movimiento_id_seq'::regclass);


--
-- TOC entry 3335 (class 0 OID 16400)
-- Dependencies: 215
-- Data for Name: tcliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.tcliente (cliente_id, direccion, edad, genero, identification, nombre, telefono, contrasena, estado) VALUES (1, 'Otavalo sn y principal', 28, 'M', '0106850076', 'Jose Lema', '098254785', '1234', true);
INSERT INTO public.tcliente (cliente_id, direccion, edad, genero, identification, nombre, telefono, contrasena, estado) VALUES (2, 'Amazonas y  NNUU', 28, 'M', '9999999999', 'Marianela Montalvo ', '097548965', '5678', true);
INSERT INTO public.tcliente (cliente_id, direccion, edad, genero, identification, nombre, telefono, contrasena, estado) VALUES (3, '13 junio y Equinoccial', 28, 'M', '9999999998', 'Juan Osorio ', '098874587', '1234', true);


--
-- TOC entry 3336 (class 0 OID 16406)
-- Dependencies: 216
-- Data for Name: tcuenta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.tcuenta (ccuenta, estado, saldo_inicial, tipo_cuenta, cliente_id) VALUES ('478758', true, 2000, 'Ahorro', 1);
INSERT INTO public.tcuenta (ccuenta, estado, saldo_inicial, tipo_cuenta, cliente_id) VALUES ('225487', true, 100, 'Corriente', 2);
INSERT INTO public.tcuenta (ccuenta, estado, saldo_inicial, tipo_cuenta, cliente_id) VALUES ('495878', true, 0, 'Ahorro', 3);
INSERT INTO public.tcuenta (ccuenta, estado, saldo_inicial, tipo_cuenta, cliente_id) VALUES ('496825', true, 540, 'Ahorro', 2);
INSERT INTO public.tcuenta (ccuenta, estado, saldo_inicial, tipo_cuenta, cliente_id) VALUES ('585545', true, 1000, 'Corriente', 1);


--
-- TOC entry 3338 (class 0 OID 16412)
-- Dependencies: 218
-- Data for Name: tmovimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (1, true, '2023-04-27 19:00:00', 2000, 'C', 2000, '478758');
INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (2, true, '2023-04-27 19:00:00', 100, 'C', 100, '225487');
INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (3, true, '2023-04-27 19:00:00', 540, 'C', 540, '496825');
INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (4, true, '2023-04-27 19:00:00', 1000, 'C', 1000, '585545');
INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (5, true, '2023-04-27 19:00:00', 1425, 'D', 575, '478758');
INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (6, true, '2023-04-27 19:00:00', 700, 'C', 600, '225487');
INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (7, true, '2023-04-27 19:00:00', 150, 'C', 150, '495878');
INSERT INTO public.tmovimiento (movimiento_id, estado, fecha, saldo, tipo_movimiento, valor, ccuenta) VALUES (8, true, '2023-04-27 19:00:00', 0, 'D', 540, '496825');


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 214
-- Name: tcliente_cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tcliente_cliente_id_seq', 3, true);


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 217
-- Name: tmovimiento_movimiento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tmovimiento_movimiento_id_seq', 8, true);


--
-- TOC entry 3185 (class 2606 OID 16405)
-- Name: tcliente tcliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcliente
    ADD CONSTRAINT tcliente_pkey PRIMARY KEY (cliente_id);


--
-- TOC entry 3187 (class 2606 OID 16410)
-- Name: tcuenta tcuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcuenta
    ADD CONSTRAINT tcuenta_pkey PRIMARY KEY (ccuenta);


--
-- TOC entry 3189 (class 2606 OID 16417)
-- Name: tmovimiento tmovimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tmovimiento
    ADD CONSTRAINT tmovimiento_pkey PRIMARY KEY (movimiento_id);


--
-- TOC entry 3191 (class 2606 OID 16423)
-- Name: tmovimiento fk77el51ui2euornd4wykl7nlh9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tmovimiento
    ADD CONSTRAINT fk77el51ui2euornd4wykl7nlh9 FOREIGN KEY (ccuenta) REFERENCES public.tcuenta(ccuenta);


--
-- TOC entry 3190 (class 2606 OID 16418)
-- Name: tcuenta fkg1qheyfdowkavqgmk9vhktuh1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tcuenta
    ADD CONSTRAINT fkg1qheyfdowkavqgmk9vhktuh1 FOREIGN KEY (cliente_id) REFERENCES public.tcliente(cliente_id);


-- Completed on 2023-04-30 13:00:36

--
-- PostgreSQL database dump complete
--

